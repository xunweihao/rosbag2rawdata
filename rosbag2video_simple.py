#!/usr/bin/env python3

"""
rosbag2video.py
rosbag to video file conversion tool
by Abel Gabor 2019
baquatelle@gmail.com
requirements:
sudo apt install python3-roslib python3-sensor-msgs python3-opencv ffmpeg
based on the tool by Maximilian Laiacker 2016
post@mlaiacker.de"""


"""
1. change topic name : ATM_PRESS_NAME, MAG_TOPIC_NAME, IMU_TOPIC_NAME
2. change input image topic name : /zed2i/zed_node/left_raw/image_raw_gray
3. input command : 
    example : python rosbag2video_simple.py --fps 100 --rate 15 -s -t /zed2i/zed_node/left_raw/image_raw_gray xxxx.bag 
    format : python rosbag2video_simple.py --fps 100 --rate 15 -s -t /zed2i/zed_node/left_raw/image_raw_gray [bag_name]
    output: save avi video, imu, magnetic, barometer raw data under path of bag_name.
"""

import roslib
#roslib.load_manifest('rosbag')
import rospy
import rosbag
import sys, getopt
import os
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
import cv2

import numpy as np

import shlex, subprocess

start_frame_id = 0
MJPEG_VIDEO = 1
RAWIMAGE_VIDEO = 2
VIDEO_CONVERTER_TO_USE = "ffmpeg" # or you may want to use "avconv"
ATM_PRESS_NAME = "/zed2i/zed_node/atm_press"
MAG_TOPIC_NAME = "/zed2i/zed_node/imu/mag"
IMU_TOPIC_NAME = "/zed2i/zed_node/imu/data_raw"


def print_help():
    print('rosbag2video.py [--fps 25] [--rate 1] [-o outputfile] [-v] [-s] [-t topic] bagfile1 [bagfile2] ...')
    print()
    print('Converts image sequence(s) in ros bag file(s) to video file(s) with fixed frame rate using',VIDEO_CONVERTER_TO_USE)
    print(VIDEO_CONVERTER_TO_USE,'needs to be installed!')
    print()
    print('--fps   Sets FPS value that is passed to',VIDEO_CONVERTER_TO_USE)
    print('        Default is 25.')
    print('-h      Displays this help.')
    print('--ofile (-o) sets output file name.')
    print('        If no output file name (-o) is given the filename \'<prefix><topic>.mp4\' is used and default output codec is h264.')
    print('        Multiple image topics are supported only when -o option is _not_ used.')
    print('        ',VIDEO_CONVERTER_TO_USE,' will guess the format according to given extension.')
    print('        Compressed and raw image messages are supported with mono8 and bgr8/rgb8/bggr8/rggb8 formats.')
    print('--rate  (-r) You may slow down or speed up the video.')
    print('        Default is 1.0, that keeps the original speed.')
    print('-s      Shows each and every image extracted from the rosbag file (cv_bride is needed).')
    print('--topic (-t) Only the images from topic "topic" are used for the video output.')
    print('-v      Verbose messages are displayed.')
    print('--prefix (-p) set a output file name prefix othervise \'bagfile1\' is used (if -o is not set).')
    print('--start Optional start time in seconds.')
    print('--end   Optional end time in seconds.')



class RosVideoWriter():
    def __init__(self, fps=25.0, rate=1.0, topic="", output_filename ="", display= False, verbose = False, start = rospy.Time(0), end = rospy.Time(sys.maxsize)):
        self.opt_topic = topic
        self.opt_out_file = output_filename
        self.opt_verbose = verbose
        self.opt_display_images = display
        self.opt_start = start
        self.opt_end = end
        self.rate = rate
        self.fps = fps
        self.opt_prefix= None
        self.t_first={}
        self.t_file={}
        self.t_video={}
        self.p_avconv = {}

    def parseArgs(self, args):
        opts, opt_files = getopt.getopt(args,"hsvr:o:t:p:",["fps=","rate=","ofile=","topic=","start=","end=","prefix="])
        for opt, arg in opts:
            if opt == '-h':
                print_help()
                sys.exit(0)
            elif opt == '-s':
                self.opt_display_images = True
            elif opt == '-v':
                self.opt_verbose = True
            elif opt in ("--fps"):
                self.fps = float(arg)
            elif opt in ("-r", "--rate"):
                self.rate = float(arg)
            elif opt in ("-o", "--ofile"):
                self.opt_out_file = arg
            elif opt in ("-t", "--topic"):
                self.opt_topic = arg
            elif opt in ("-p", "--prefix"):
                self.opt_prefix = arg
            elif opt in ("--start"):
                self.opt_start = rospy.Time(int(arg))
                if(self.opt_verbose):
                    print("starting at",self.opt_start.to_sec())
            elif opt in ("--end"):
                self.opt_end = rospy.Time(int(arg))
                if(self.opt_verbose):
                    print("ending at",self.opt_end.to_sec())
            else:
                print("opz:", opt,'arg:', arg)

        if (self.fps<=0):
            print("invalid fps", self.fps)
            self.fps = 1

        if (self.rate<=0):
            print("invalid rate", self.rate)
            self.rate = 1

        if(self.opt_verbose):
            print("using ",self.fps," FPS")
        return opt_files


    # filter messages using type or only the opic we whant from the 'topic' argument
    def filter_image_msgs(self, topic, datatype, md5sum, msg_def, header):
        if(datatype=="sensor_msgs/CompressedImage"):
            if (self.opt_topic != "" and self.opt_topic == topic) or self.opt_topic == "":
                print("############# COMPRESSED IMAGE  ######################")
                print(topic,' with datatype:', str(datatype))
                print()
                return True;

        if(datatype=="theora_image_transport/Packet"):
            if (self.opt_topic != "" and self.opt_topic == topic) or self.opt_topic == "":
                print(topic,' with datatype:', str(datatype))
                print('!!! theora is not supported, sorry !!!')
                return False;

        if(datatype=="sensor_msgs/Image"):
            if (self.opt_topic != "" and self.opt_topic == topic) or self.opt_topic == "":
                print("############# UNCOMPRESSED IMAGE ######################")
                print(topic,' with datatype:', str(datatype))
                print()
                return True;

        return False;


    def write_output_video(self, msg, cv_image, topic, t, video_fmt, pix_fmt = ""):
        if self.opt_out_file=="":
            out_file = self.opt_prefix + str(topic).replace("/", "_")+".avi"
        else:
            out_file = self.opt_out_file
        print("out_file", out_file)
        global start_frame_id
        global video
        if(start_frame_id == 0):
            video = cv2.VideoWriter(out_file, 0, 25, (320,240))
            start_frame_id=start_frame_id+1
        
        start_frame_id=start_frame_id+1

        if(int(start_frame_id) % 8 == 0):
            font                   = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10,30)
            fontScale              = 1
            fontColor              = (0,0,255)
            thickness              = 2
            lineType               = 2

            cv2.putText(cv_image, str(t), 
            bottomLeftCornerOfText, 
            font, 
            fontScale,
            fontColor,
            thickness)
            cv_image = cv2.resize(cv_image, (int(320), int(240)), interpolation=cv2.INTER_AREA)
            video.write(cv_image)

    def addBag(self, filename):
        if self.opt_display_images:
            from cv_bridge import CvBridge, CvBridgeError
            bridge = CvBridge()
            cv_image = []

        if self.opt_verbose :
            print("Bagfile: {}".format(filename))

        if not self.opt_prefix:
            # create the output in the same folder and name as the bag file minu '.bag'
            self.opt_prefix = bagfile[:-4]

        #Go through the bag file
        bag = rosbag.Bag(filename)
        if self.opt_verbose :
            print("Bag opened.")
        # loop over all topics
        for topic, msg, t in bag.read_messages(connection_filter=self.filter_image_msgs, start_time=self.opt_start, end_time=self.opt_end):
            try:
                if msg.format.find("jpeg")!=-1 :
                    if msg.format.find("8")!=-1 and (msg.format.find("rgb")!=-1 or msg.format.find("bgr")!=-1 or msg.format.find("bgra")!=-1 ):
                        if self.opt_display_images:
                            np_arr = np.fromstring(msg.data, np.uint8)
                            cv_image = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
                        self.write_output_video( msg, cv_image, topic, t, MJPEG_VIDEO )
                    elif msg.format.find("mono8")!=-1 :
                        if self.opt_display_images:
                            np_arr = np.fromstring(msg.data, np.uint8)
                            cv_image = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
                        self.write_output_video( msg, cv_image, topic, t, MJPEG_VIDEO )
                    elif msg.format.find("16UC1")!=-1 :
                        if self.opt_display_images:
                            np_arr = np.fromstring(msg.data, np.uint16)
                            cv_image = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
                        self.write_output_video( msg, cv_image, topic, t, MJPEG_VIDEO )
                    else:
                        print('unsupported jpeg format:', msg.format, '.', topic)

            # has no attribute 'format'
            except AttributeError:
                try:
                        pix_fmt=None
                        if msg.encoding.find("mono8")!=-1 or msg.encoding.find("8UC1")!=-1:
                            pix_fmt = "gray"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")

                        elif msg.encoding.find("bgra")!=-1 :
                            pix_fmt = "bgra"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")

                        elif msg.encoding.find("bgr8")!=-1 :
                            pix_fmt = "bgr24"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
                        elif msg.encoding.find("bggr8")!=-1 :
                            pix_fmt = "bayer_bggr8"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bayer_bggr8")
                        elif msg.encoding.find("rggb8")!=-1 :
                            pix_fmt = "bayer_rggb8"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bayer_rggb8")
                        elif msg.encoding.find("rgb8")!=-1 :
                            pix_fmt = "rgb24"
                            if self.opt_display_images:
                                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
                        elif msg.encoding.find("16UC1")!=-1 :
                            pix_fmt = "gray16le"
                        else:
                            print('unsupported encoding:', msg.encoding, topic)
                            #exit(1)
                        if pix_fmt:
                            self.write_output_video( msg, cv_image, topic, t, RAWIMAGE_VIDEO, pix_fmt )

                except AttributeError:
                    # maybe theora packet
                    # theora not supported
                    if self.opt_verbose :
                        print("Could not handle this format. Maybe thoera packet? theora is not supported.")
                    pass

            if self.opt_display_images:
                cv2.imshow(topic, cv_image)
                key=cv2.waitKey(1)
                if key==1048603:
                    exit(1)
        if self.p_avconv == {}:
            print("No image topics found in bag:", filename)
        bag.close()



if __name__ == '__main__':
    #print()
    #print('rosbag2video, by Maximilian Laiacker 2020 and Abel Gabor 2019')
    #print()

    if len(sys.argv) < 2:
        print('Please specify ros bag file(s)!')
        print_help()
        sys.exit(1)
    else :
        videowriter = RosVideoWriter()
        try:
            opt_files = videowriter.parseArgs(sys.argv[1:])
            print("opt_files=",opt_files)
        except getopt.GetoptError:
            print_help()
            sys.exit(2)

   
    
    print("opt_files=",opt_files[0])
    bag = rosbag.Bag(opt_files[0])
    save_imu_txt_name = (sys.argv[8].replace(".bag","")+"_IMU_RAW_DATA.txt");
    save_mag_txt_name = (sys.argv[8].replace(".bag","")+"_MAG_RAW_DATA.txt");
    save_baro_txt_name = (sys.argv[8].replace(".bag","")+"_BARO_RAW_DATA.txt");

    # # extract barameter from ZED2i
    with open(str(save_baro_txt_name), "w") as f:
        for topic, msg, t in bag.read_messages(topics=[ATM_PRESS_NAME]):
            # print("ATM_PRESS_NAME:",t.to_sec())
            # print("atm_msg:",msg.fluid_pressure)
            # print("atm_variance:",msg.variance)
            f.write(str((('%.9f' % t.to_sec()))))
            f.write(" ")
            f.write(str(msg.fluid_pressure))
            f.write(" ")
            f.write(str(msg.variance))
            f.write("\n")

 
    with open(str(save_mag_txt_name), "w") as f2:
        for topic, msg, t in bag.read_messages(topics=[MAG_TOPIC_NAME]):
            # print("MAG_TOPIC_NAME:",t.to_sec())
            # print("MAG_TOPIC_NAME:",msg)
            # print("atm_msg:",msg.fluid_pressure)
            # print("atm_variance:",msg.variance)
            f2.write(str((('%.9f' % t.to_sec()))))
            f2.write(" ")
            f2.write(str(msg.magnetic_field.x))
            f2.write(" ")
            f2.write(str(msg.magnetic_field.y))
            f2.write(" ")
            f2.write(str(msg.magnetic_field.z))
            f2.write("\n")
    

    # #extract imu from ZED2i
    with open(str(save_imu_txt_name), "w") as f3:
        for topic, msg, t in bag.read_messages(topics=[IMU_TOPIC_NAME]):
            #print("IMU_TOPIC_NAME:",t.to_sec())
            #print("IMU_TOPIC_NAME:",msg)
            f3.write(str((('%.9f' % t.to_sec()))))
            f3.write(" ")
            f3.write(str(msg.angular_velocity.x))
            f3.write(" ")
            f3.write(str(msg.angular_velocity.y))
            f3.write(" ")
            f3.write(str(msg.angular_velocity.z))
            f3.write(" ")
            f3.write(str(msg.linear_acceleration.x))
            f3.write(" ")
            f3.write(str(msg.linear_acceleration.y))
            f3.write(" ")
            f3.write(str(msg.linear_acceleration.z))

            f3.write("\n")

    # loop over all files
    for files in range(0,len(opt_files)):
        #First arg is the bag to look at
        bagfile = opt_files[files]
        videowriter.addBag(bagfile)
    print("finished")

